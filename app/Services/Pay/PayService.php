<?php
/**
 * Created by lebron
 * User: lebron
 * Date: 2020/3/12
 * Time: 16:27
 */

namespace App\Services\Pay;

abstract class PayService
{
    protected $payIns;

    /**
     * 支付
     *
     * @param $order
     * @param $payMethod
     *
     * @return mixed
     * @throws \Exception
     */
    public function pay($order, $payMethod)
    {
        if (!in_array($payMethod, $this->payMethod)) {
            throw new \Exception("不支持的支付方式");
        }

        return $this->payIns->$payMethod($order);
    }

    /**
     * 查询订单
     *
     * @param        $order
     * @param string $type
     */
    public function find($order, $type = 0)
    {
        if (empty($order['out_trade_no'])) throw new \Exception("缺少参数：out_trade_no");

        switch($type) {
            // 查询普通支付订单
            case 0:
                $res = $this->payIns->find($order);
                break;
            // 查询退款订单
            case 'refund':
                // TODO 支付宝退款还需要参数 $order['out_request_no'])
                $res = $this->payIns->find($order, 'refund');
                break;
            // 查询转账订单
            case 'transfer':
                $res = $this->payIns->find($order, 'transfer');
                break;
            default:
                throw new \Exception("不存在的查询方式");
        }

        return $res;
    }
}

<?php
/**
 * Created by lebron
 * User: lebron
 * Date: 2020/3/13
 * Time: 19:39
 */

namespace App\Models\V1;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = 'users';

}

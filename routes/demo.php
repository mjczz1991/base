<?php

use App\Services\GlobalOptionsService;
use App\Services\ValidatorService;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Exceptions\ApiException;
use Laravel\Passport\Passport;

Route::get("/", function (Request $request) {
    return [
        'code' => 200,
        'data' => 'demo'
    ];
});

// passport颁发令牌
Route::get("/issueAccessToken", function (Request $request) {
    $user = \App\Models\V1\User::find(1);
    $result = $user->createToken("api_token");
    return $result;
});

// 获取通过passport认证的用户
Route::get('/user', function () {
    return Auth::user();
})->middleware("auth:api");

// 获取数据字典
Route::get("/getOptions", function (Request $request) {
    return [
        'code' => 200,
        'data' => GlobalOptionsService::getOptions($request->get("key", null))
    ];
});

// 颁发API令牌#
Route::post('/airlock/token', function (Request $request) {
    $rules = [
        'email' => 'required|email',
        'password' => 'required',
        'device_name' => 'required'
    ];

    if ($errMessage = ValidatorService::validateParams($rules)) throw new ApiException($errMessage);

    $user = User::where('email', $request->email)->first();

    if (! $user || ! Hash::check($request->password, $user->password)) {
        throw new ApiException("The provided credentials are incorrect.");
    }

    return [
        'code' => 200,
        'data' => $user->createToken($request->device_name)->plainTextToken
    ];
});

// 注册用户
Route::post("/user/register", function (Request $request) {
    $rules = [
        'email' => 'required|email',
        'password' => 'required',
        'device_name' => 'required'
    ];

    if ($errMessage = ValidatorService::validateParams($rules)) throw new ApiException($errMessage);

    $user = User::where('email', $request->email)->first();

    if ($user) throw new ApiException("用户已存在");

    $user = User::create([
        'name' => $request['email'],
        'email' => $request['email'],
        'password' => bcrypt($request['password']),
        'device_name' => $request['device_name'],
    ]);

    // 创建token
    $user['token'] = $user->createToken($request->device_name)->plainTextToken;

    return [
        'code' => 200,
        'data' => $user
    ];
});

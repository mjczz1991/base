<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (strpos($request->route()->getPrefix(), 'v1') !== false) {
            return route('apiUnAuth');
        }

        if (strpos($request->route()->getPrefix(), 'demo') !== false) {
            return route('apiUnAuth');
        }

        //eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNjIzYmU5NDRlOTc5ZmU2YmE1ZWVhY2Y3ZjVkYWZmNWQ3OTM2MTk0NDc3ZDNmMzE1YWE4MGRiZDllNTkyYTMxNjJiODI3MjUzZGVmNTNkYzMiLCJpYXQiOjE1ODQxMDE4NzksIm5iZiI6MTU4NDEwMTg3OSwiZXhwIjoxNjE1NjM3ODc5LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.LLEIxb0BfDRogAukb5YVv2OrOAKgJOS2574_flQR9apBdHnuUkEaBWBiX5g8ThGmh-CtGV2RhaR-qniWmtKP20ZTq2WiAtna1_P8C0UmDyg84WUUG6MuXPAVrIeBXfZuQ8a_q2JVWEhYyzE7AuepAJQPWTfaR5m-dpIR03TI99UHQGitUzumLjj7wVNf_VJ9AldmW9WfEfuUINMv1TEJCgncxNz6hKVP6px74pLdPHd4hsFnpCBMYrRtcmAv6-k1InJ9VY8wzFr5TmwX0qWvHafCHK8TFC9aKHMc8UJMfasdEqHgtQDlMlvKk1sbVJxxf8Jc2myWKhE2BCNp9ReIfIo1mKXyaN-SuaAjdbkhMHUQR4V2X5jn-WEyB2fNFSvgymfXuINc6OglABpL-5v5iB9TzJP2-H_vKIeouPdajvGO6jcGJdJyIVaUsRxHDkCASs5cirIWivCZKmOwjqRsHmQyD4yLEiAIJjCPlHOMwPEqLKyZ43bf8O9eBjlUZfA2StlqRRN5zqkzdJFxasoCvBkqx0yG3KGMAPvfEeTWN93YpqwmL6KoFwtPplNSNCjZtl5cghQzQ27cJ5jiFTenRj_GZ2ZxTdcuGdFeLr3GgFuk2-p-WiG21K2npzn_KRogPobINCicOBbeb3IMSodThnNWiXStgnYK0fU6Q0bLtMA

        //if (! $request->expectsJson()) {
            //return route('login');
        //}
    }
}

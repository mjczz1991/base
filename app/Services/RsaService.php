<?php

namespace App\Services;

class RsaService
{
    /**
     * 私钥签名
     *
     * @param $data
     *
     * @return string
     */
    public static function sign($data, $private_key = null, $signature_alg = OPENSSL_ALGO_SHA1)
    {
        $private_key = !empty($private_key) ? $private_key : config('rsa.private_key');

        openssl_sign($data, $signature, $private_key, $signature_alg);

        $signature = base64_encode($signature);

        return $signature;
    }

    /**
     * 公钥验签
     *
     * @param $data
     * @param $signature
     *
     * @return int
     */
    public static function verify($data, $signature, $public_key = null, $signature_alg = OPENSSL_ALGO_SHA1)
    {
        $public_key = !empty($public_key) ? $public_key : config('rsa.public_key');

        return openssl_verify($data, base64_decode($signature), $public_key);
    }

}

<?php
/**
 * Created by czz.
 * User: czz
 * Date: 2020/4/13
 * Time: 19:21
 */

namespace App\Services;


use Predis\ClientInterface;

/**
 * Class RedisService
 *
 * @package App\Services
 */
class RedisService
{
    /**
     * @var null
     */
    public static $ins = null;


    /**
     * redis客户端
     *
     * @var ClientInterface;
     */
   public $client;

    /**
     * 实例化
     *
     * RedisService constructor.
     *
     * @param array $config
     */
    public final function __construct($config = [])
    {
        $this->client = new \Predis\Client($config);
    }

    /**
     * 获取单例
     *
     * @return RedisService
     */
    public static function getInstance()
    {
        if (self::$ins === null) self::$ins = new self(config("database.redis.default"));

        return self::$ins;
    }

    public static function redis()
    {
        return self::getInstance()->client;
    }

}

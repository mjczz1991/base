<?php
/**
 * Created by lebron
 * User: lebron
 * Date: 2020/3/12
 * Time: 9:50
 */

namespace App\Services\Pay;

use App\Services\ValidatorService;
use Illuminate\Http\Request;
use Yansongda\Pay\Pay;
use Yansongda\Pay\Log;

class WxPayService extends PayService
{
    // mp	公众号支付
    // miniapp	小程序支付
    // wap	H5 支付
    // scan	扫码支付
    // pos	刷卡支付
    // app	APP 支付
    // transfer	企业付款
    // redpack	普通红包
    // groupRedpack	分裂红包
    protected $payMethod = ['mp', 'miniapp', 'wap', 'scan', 'pos', 'app', 'transfer', 'redpack', 'groupRedpack'];

    /**
     * WxPayService constructor.
     *
     * @param string $configKey
     * @param string $notifyUrl
     *
     * @throws \Exception
     */
    public function __construct($configKey = 'wechat', $notifyUrl = '')
    {
        $config = self::getConfig(!empty($configKey) ? $configKey : 'wechat');

        !empty($notifyUrl) && $config['notify_url'] = $notifyUrl;

        $this->payIns = Pay::wechat($config);
    }

    /**
     * @param $configKey
     *
     * @return \Illuminate\Config\Repository|mixed
     * @throws \Exception
     */
    public static function getConfig($configKey)
    {
        $config = config("pay.".$configKey);

        if (empty($config)) throw new \Exception("配置不存在");

        return $config;
    }

    /**
     * 公众号支付
     *
     * @param Request $request
     *
     * @return \Yansongda\Supports\Collection
     * @throws \Exception
     */
    public function mpPay(Request $request)
    {
        $rules = [
            'out_trade_no' => 'required',
            'body' => 'required',
            'total_fee' => 'required',
            'openid' => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'out_trade_no' => $request['out_trade_no'],
            'body' => $request['body'],
            'total_fee' => intval($request['total_fee'] * 100),
            'openid' => $request['openid'],
        ];

        // 返回 Collection 实例。包含了调用 JSAPI 的所有参数，如appId，timeStamp，nonceStr，package，signType，paySign 等；
        // 可直接通过 $result->appId, $result->timeStamp 获取相关值。
        // 后续调用不在本文档讨论范围内，请自行参考官方文档。
        return $this->payIns->mp($order);
    }

    /**
     * 手机网站支付、H5支付
     *
     * @param Request $request
     *
     * @return \Yansongda\Supports\Collection
     * @throws \Exception
     */
    public function wapPay(Request $request)
    {
        $rules = [
            'out_trade_no' => 'required',
            'body' => 'required',
            'total_fee' => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'out_trade_no' => $request['out_trade_no'],
            'body' => $request['body'],
            'total_fee' => intval($request['total_fee'] * 100),
        ];

        return $this->payIns->wap($order);
    }

    /**
     * APP支付
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function appPay(Request $request)
    {
        $rules = [
            'out_trade_no' => 'required',
            'body' => 'required',
            'total_fee' => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'out_trade_no' => $request['out_trade_no'],
            'body' => $request['body'],
            'total_fee' => intval($request['total_fee'] * 100),
        ];

        return $this->payIns->app($order);
    }

    /**
     * 刷卡支付
     *
     * @param Request $request
     *
     * @return \Yansongda\Supports\Collection
     * @throws \Exception
     */
    public function posPay(Request $request)
    {
        $rules = [
            'out_trade_no' => 'required',
            'body' => 'required',
            'total_fee' => 'required',
            'auth_code' => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'out_trade_no' => $request['out_trade_no'],
            'body' => $request['body'],
            'total_fee' => intval($request['total_fee'] * 100),
            'auth_code' => $request['auth_code'],
        ];

        return $this->payIns->pos($order);
    }

    /**
     * 扫码支付
     *
     * @param Request $request
     *
     * @return \Yansongda\Supports\Collection
     * @throws \Exception
     */
    public function scanPay(Request $request)
    {
        $rules = [
            'out_trade_no' => 'required',
            'body' => 'required',
            'total_fee' => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'out_trade_no' => $request['out_trade_no'],
            'body' => $request['body'],
            'total_fee' => intval($request['total_fee'] * 100),
        ];

        // 扫码支付使用 模式二
        // 二维码内容： $qr = $result->code_url;
        return $this->payIns->scan($order);
    }

    /**
     * 账户转账
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function transferPay(Request $request)
    {
        $rules = [
            'partner_trade_no' => 'required',
            'openid' => 'required',
            //'check_name' => 'required',
            // 're_user_name'=>'张三', //check_name为 FORCE_CHECK 校验实名的时候必须提交
            'amount' => 'required',
            'desc' => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'partner_trade_no' => $request['partner_trade_no'], // 商户订单号
            'openid' => $request['openid'], // 收款人的openid
            'check_name' => 'NO_CHECK', // NO_CHECK：不校验真实姓名\FORCE_CHECK：强校验真实姓名
            // 're_user_name'=>'张三',  // check_name为 FORCE_CHECK 校验实名的时候必须提交
            'amount' => intval($request['amount'] * 100), // 企业付款金额，单位为分
            'desc' => $request['desc'], // 付款说明
        ];

        return $this->payIns->transfer($order);
    }

    /**
     * 小程序支付
     *
     * @param Request $request
     *
     * @return \Yansongda\Supports\Collection
     * @throws \Exception
     */
    public function miniappPay(Request $request)
    {
        $rules = [
            'out_trade_no' => 'required',
            'body' => 'required',
            'total_fee' => 'required',
            'openid' => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'out_trade_no' => $request['out_trade_no'],
            'body' => $request['body'],
            'total_fee' => intval($request['total_fee'] * 100),
            'openid' => $request['openid'],
        ];

        // 返回 Collection 实例。包含了调用 JSAPI 的所有参数，如appId，timeStamp，nonceStr，package，signType，paySign 等；
        // 可直接通过 $result->appId, $result->timeStamp 获取相关值。
        // 后续调用不在本文档讨论范围内，请自行参考官方文档。
        return $this->payIns->miniapp($order);
    }

    /**
     * 普通红包支付
     * @param Request $request
     *
     * @return \Yansongda\Supports\Collection
     * @throws \Exception
     */
    public function redpackPay(Request $request)
    {
        $rules = [
            'mch_billno' => 'required',
            'send_name' => 'required',
            'total_amount' => 'required',
            're_openid' => 'required',
            'total_num' => 'required',
            'wishing' => 'required',
            'act_name' => 'required',
            'remark' => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'mch_billno' => $request['mch_billno'],
            'send_name' => $request['send_name'],
            'total_amount' => intval($request['total_amount'] * 100),
            're_openid' => $request['re_openid'],
            'total_num' => $request['total_num'],
            'wishing' => $request['wishing'],
            'act_name' => $request['act_name'],
            'remark' => $request['remark'],
        ];

        return $this->payIns->redpack($order);
    }

    /**
     * 裂变红包支付
     *
     * @param Request $request
     *
     * @return \Yansongda\Supports\Collection
     * @throws \Exception
     */
    public function groupRedpackPay(Request $request)
    {
        $rules = [
            'mch_billno' => 'required',
            'send_name' => 'required',
            'total_amount' => 'required',
            're_openid' => 'required',
            'total_num' => 'required',
            'wishing' => 'required',
            'act_name' => 'required',
            'remark' => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'mch_billno' => $request['mch_billno'],
            'send_name' => $request['send_name'],
            'total_amount' => intval($request['total_amount'] * 100),
            're_openid' => $request['re_openid'],
            'total_num' => $request['total_num'],
            'wishing' => $request['wishing'],
            'act_name' => $request['act_name'],
            'remark' => $request['remark'],
        ];

        return $this->payIns->groupRedpack($order);
    }

    /**
     * 退款
     *
     * @param Request $request
     *
     * @return mixed|\Yansongda\Supports\Collection
     * @throws \Yansongda\Pay\Exceptions\GatewayException
     * @throws \Yansongda\Pay\Exceptions\InvalidArgumentException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     */
    public function refund(Request $request)
    {
        $rules = [
            'out_trade_no' => 'required',
            'total_fee' => 'required',
            'refund_fee' => 'required',
            'refund_desc' => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'out_trade_no' => $request['out_refund_no'],
            'out_refund_no' => !empty($request['out_refund_no']) ? $request['out_refund_no'] : time(),
            'total_fee' => $request['total_fee'],
            'refund_fee' => $request['refund_fee'],
            'refund_desc' => $result['refund_desc'],
        ];

        // APP/小程序的订单，请传入参数：['type' => 'app']/['type' => 'miniapp']
        !empty($request['type']) && $order['type'] = $request['type'];

        return $this->payIns->refund($order);
    }

    /**
     * 支付回调
     *
     * @param string   $configKey
     * @param string   $notifyUrl
     * @param \Closure $logic
     *
     * @return mixed
     * @throws \Exception
     */
    public function notify($configKey = 'wechat', $notifyUrl = '', \Closure $logic)
    {
        $wxpay = (new self($configKey, $notifyUrl))->payIns;

        try{
            // 验签
            $data = $wxpay->verify();

            // TODO
            // 1、对data数据中的字段进行判断，判断是否支付成功

            // 2、业务逻辑
            $logic();

            Log::debug('Wechat notify', $data->all());
        } catch (\Exception $e) {
            Log::debug('Wechat notify Exception', $e->getMessage());
            // throw new \Exception($e);
            // $e->getMessage();
        }

        return $wxpay->success();
    }
}

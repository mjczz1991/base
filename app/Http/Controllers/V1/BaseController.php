<?php
/**
 * @author Lebron
 */

namespace App\Http\Controllers\V1;

use App\Services\ValidatorService;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    const OK              = 200;
    const BAD_REQUEST     = 400;
    const UNAUTHORIZED    = 401;
    protected $defaultPage = 1;
    protected $defaultPageSize = 20;

    /**
     * 验证请求参数信息
     *
     * @param array $rules 验证规则
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public static function validateParams($rules, $code = self::BAD_REQUEST)
    {
        if ($errMessage = ValidatorService::validateParams($rules)) return self::fail($errMessage, $code);

        return false;
    }

    /**
     * 返回错误
     *
     * @param string $message
     * @param int    $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function fail($message = 'fail', $code = self::BAD_REQUEST)
    {
        $body['error'] = true;
        $body['message'] = $message;

        return self::json($body, $code);
    }

    /**
     * 返回操作成功
     *
     * @param array  $data
     * @param int    $code
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function success($message = 'success', $code = self::OK)
    {
        $body['error'] = false;
        $body['message'] = $message;

        return self::json($body, $code);
    }

    /**
     * 返回数据，数据包裹在data下
     *
     * @param     $body
     * @param int $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function response($body, $code = self::OK)
    {
        if (config('app.debug')) $body = self::debug($body);

        return response()->json([
            'code' => $code,
            'data' => $body
        ]);
    }

    /**
     * 返回json数据
     *
     * @param bool|array $body
     * @param int $code 状态码
     * @return \Illuminate\Http\JsonResponse
     */
    public static function json($body = false, $code = self::OK)
    {
        $body['code'] = $code;

        if (config('app.debug')) $body = self::debug($body);

        return response()->json($body);
    }

    public static function debug($body)
    {
        $debug_id = uniqid();

        parse_str(Request::getContent(), $post_field);

        \Log::debug($debug_id, [
            'LOG_ID'         => $debug_id,
            'IP_ADDRESS'     => Request::ip(),
            'REQUEST_URL'    => Request::fullUrl(),
            'AUTHORIZATION'  => Request::header('Authorization'),
            'REQUEST_METHOD' => Request::method(),
            'PARAMETERS'     => ['query_string' => Request::getQueryString(), 'post_fields' => $post_field],
            'RESPONSES'      => $body
        ]);

        is_array($body) && $body['debug_id'] = $debug_id;

        return $body;
    }

    public function unAuth () {
        return self::json(['message' => '未认证'], self::UNAUTHORIZED);
    }
}

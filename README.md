# Laravel 基础项目

## php依赖扩展

- PHP >= 7.2.5
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

## 安装

```
cp .env.example .env
composer self-update 
composer install
php artisan key:generate
```

## 开发规范
见doc目录下

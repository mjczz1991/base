<?php
/**
 * Created by czz.
 * User: czz
 * Date: 2020/3/7
 * Time: 20:27
 */

namespace App\Services;

class DbService
{
    /**
     * 在数据库事务下运行该方法,简化事务的写法。
     *
     * @param Closure $run
     *
     * @throws \Exception
     */
    public static function runOnTransaction(\Closure $run)
    {
        try {
            \DB::beginTransaction();
            $run();
        } catch(\Exception $e) {
            \DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * TODO 获取最后一条查询的sql
     *
     */
    public static function showLastSql()
    {

    }

}

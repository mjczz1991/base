### 数据库建表原则

- 字符串字段设置默认值空字符串：‘’
- 整型字段设置默认值：0
- 字段代表意义从1开始，如：status 默认0, 1有效，2无效
- 字段需备注意义

***使用migration建表***
```php
<?php
Schema::create('test', function (Blueprint $table) {
    $table->increments('id');
    $table->integer('user_id')->default(0)->comment('用户id');
    $table->string('path')->default('')->comment('路径');
    $table->string('method', 10)->default('')->comment('方法');
    $table->string('ip')->default('')->comment('ip地址');
    $table->text('input')->nullable()->comment('input');
    $table->integer('cuid')->default(0)->comment('添加人');
    $table->integer('muid')->default(0)->comment('修改人');
    $table->index('user_id');
    $table->timestamps();
});
```

***对应生成的sql为***
```sql
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '路径',
  `method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '方法',
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'ip地址',
  `input` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'input',
  `cuid` int(11) NOT NULL DEFAULT 0 COMMENT '添加人',
  `muid` int(11) NOT NULL DEFAULT 0 COMMENT '修改人',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `test_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;
```

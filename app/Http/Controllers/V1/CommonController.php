<?php
/**
 * Created by lebron
 * User: lebron
 * Date: 2020/3/13
 * Time: 9:58
 */

namespace App\Http\Controllers\V1;

use App\Services\GlobalOptionsService;
use Illuminate\Http\Request;

class CommonController extends BaseController
{
    /**
     * 获取数据字典
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOptions(Request $request)
    {
        return self::response(GlobalOptionsService::getOptions($request->get("key", null)));
    }

}

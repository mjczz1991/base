<?php

namespace App\Services;

class UtillService
{
    /**
     * 字符串加*号隐藏
     *
     * @param $user_name
     *
     * @return string
     */
    public static function substrCut($user_name)
    {
        // 获取字符串长度
        $strlen = mb_strlen($user_name, 'utf-8');

        // 如果字符创长度小于2，不做任何处理
        if ($strlen < 2) {
            return $user_name;
        } else {
            // mb_substr — 获取字符串的部分
            $firstStr = mb_substr($user_name, 0, 1, 'utf-8');
            $lastStr = mb_substr($user_name, -1, 1, 'utf-8');

            // str_repeat — 重复一个字符串
            return $strlen == 2
                ? $firstStr . str_repeat('*', mb_strlen($user_name, 'utf-8') - 1)
                : $firstStr . str_repeat("*", $strlen - 2) . $lastStr;
        }
    }

    /**
     * 获取随机字符串。符合一般字符规则
     *
     * @param int $length 随机字符串长度
     * @param bool $unique 是否生成唯一（提高不重复性）
     * @return string
     */
    public static function getRandomString($length, $unique = false)
    {
        $chars = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $charsMaxIndex = strlen($chars) - 1;

        $retStr = "";
        if ($unique) {
            $retStr = uniqid();
            $strLen = strlen($retStr);
            if ($strLen > $length) {
                return StringHelper::byteSubstr(0, $length);
            }
            $length -= $strLen;
        }

        while ($length-- > 0) {
            $retStr .= $chars[rand(0, $charsMaxIndex)];
        }

        return $retStr;
    }

    /**
     * 获取数组中的随机元素
     *
     * @param array $array
     * @return mixed
     */
    public static function getRandCell(array $array)
    {
        if (count($array) === 0) return null;

        return $array[rand(0, count($array) - 1)];
    }


}

<?php
/**
 * Created by lebron
 * User: lebron
 * Date: 2020/3/8
 * Time: 14:04
 */

namespace App\Http\Controllers\V1;


use App\Exceptions\ApiException;
use App\Services\ValidatorService;
use App\Models\V1\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Passport;

class AuthController extends BaseController
{
    /**
     * 登录
     *
     * @param Request $request
     *
     * @return array
     * @throws ApiException
     */
    public function login(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
            'device_name' => 'required'
        ];

        if ($errMessage = ValidatorService::validateParams($rules)) throw new ApiException($errMessage);

        $user = User::where('email', $request->email)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw new ApiException("The provided credentials are incorrect.");
        }

        // 颁发个人访问令牌
        $result = $user->createToken("api_token");

        return [
            'code' => 200,
            'data' => $result->accessToken,
        ];
    }

}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '上线';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            chdir(dirname(dirname(dirname(__DIR__))));

            echo PHP_EOL . "====== 开始 git pull master ======" . PHP_EOL;
            system("git pull origin");
            echo PHP_EOL . "====== 完成 git pull master ======" . PHP_EOL . PHP_EOL;

            echo PHP_EOL . "====== 开始 升级数据库 ======" . PHP_EOL;
            system("php artisan migrate");
            echo PHP_EOL . "====== 完成 升级数据库 ======" . PHP_EOL . PHP_EOL;

            echo PHP_EOL . "====== 开始 composer install ======" . PHP_EOL;
            system("composer install");
            echo PHP_EOL . "====== 完成 composer install ======" . PHP_EOL . PHP_EOL;

        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}

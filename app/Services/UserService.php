<?php
/**
 * Created by czz.
 * User: czz
 * Date: 2020/4/13
 * Time: 20:30
 */
# +-------------------------------------------------------------------
#
#             ┏┓      ┏┓
#            ┏┛┻━━━━━━┛┻┓
#            ┃               ☃              ┃
#            ┃  ┳┛   ┗┳ ┃
#            ┃     ┻    ┃
#            ┗━┓      ┏━┛
#              ┃      ┗━━━━━┓
#              ┃  神兽保佑              ┣┓
#              ┃ 永无BUG！            ┏┛
#              ┗┓┓┏━┳┓┏━━━━━┛
#               ┃┫┫ ┃┫┫
#

namespace App\Services;


class UserService
{
    protected static $userKeyPrefix = "hash:user:";

    /**
     * 添加用户
     *
     * @param array $user
     *
     * @return mixed
     */
    public static function addUser($user = [])
    {
        RedisService::redis()->hmset(self::$userKeyPrefix.$user['id'], $user);
        //return RedisService::redis()->transaction(function() use ($user) {
        //    RedisService::redis()->zadd("rank_user", $user['id'], $user['name']);
        //});
    }

    /**
     * 获取用户
     *
     * @param $userId
     *
     * @return array
     */
    public static function getUser($userId)
    {
        return RedisService::redis()->hgetall(self::$userKeyPrefix.$userId);
    }

    /**
     * 获取所有用户
     *
     * @param string $userKeyPrefix
     */
    public static function getAllUser()
    {
        return RedisService::getInstance()->client->hgetall("*");

    }

}

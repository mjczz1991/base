<?php
/**
 * Created by lebron
 * User: lebron
 * Date: 2020/3/12
 * Time: 15:55
 */

namespace App\Http\Controllers\V1;

use App\Services\GlobalOptionsService;
use App\Services\Pay\AlipayService;
use App\Services\Pay\WxPayService;
use Illuminate\Http\Request;

class PayController extends BaseController
{
    /**
     * 支付
     *
     * @param Request $request
     *
     * @return bool|\Illuminate\Http\JsonResponse|mixed
     */
    public function pay(Request $request)
    {
        $rules = [
            'type' => 'required',
            'pay_method' => 'required',
        ];

        if ($err = self::validateParams($rules)) return $err;

        try {
            if ($request['type'] == 1) $payService = new WxPayService($request->get("config_key", null));
            if ($request['type'] == 2) $payService = new AlipayService($request->get("config_key", null));

            // 调用对应支付方法
            $payMethod = $request['pay_method']."Pay";
            $res = $payService->$payMethod($request);

            return $res;
        } catch(\Exception $e) {

            return self::fail($e->getMessage());
        } catch(Throwable $e) {

            return self::fail($e->getMessage());
        }

    }

    /**
     * 退款
     *
     * @param Request $request
     */
    public function refund(Request $request)
    {
        $rules = [
            'type' => 'required',
            'out_trade_no' => 'required',
            'refund_money' => 'required',
        ];

        if ($err = self::validateParams($rules)) return $err;

        try {
            if ($request['type'] == 1) $payService = new WxPayService($request->get("config_key", null));
            if ($request['type'] == 2) $payService = new AlipayService($request->get("config_key", null));

            $res = $payService->refund($request);

            return $res;
        } catch(\Exception $e) {

            return self::fail($e->getMessage());
        } catch(Throwable $e) {

            return self::fail($e->getMessage());
        }

    }
}

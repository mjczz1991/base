<?php
/**
 * Created by lebron
 * User: lebron
 * Date: 2020/3/12
 * Time: 9:50
 */

namespace App\Services\Pay;

use App\Services\ValidatorService;
use Illuminate\Http\Request;
use Yansongda\Pay\Pay;
use Yansongda\Pay\Log;

class AlipayService extends PayService
{
    /**
     * 支持的支付方法
     *
     * web  电脑支付
     * wap  手机网站支付
     * app  APP 支付
     * pos  刷卡支付
     * scan 扫码支付
     * transfer	帐户转账
     * mini	小程序支付
     *
     * @var array
     */
    protected $payMethod = ['web', 'wap', 'app', 'pos', 'scan', 'transfer', 'mini'];

    /**
     * AlipayService constructor.
     *
     * @param string $configKey
     * @param string $notifyUrl 异步通知地址
     *
     * @throws \Exception
     */
    public function __construct($configKey = 'alipay', $notifyUrl = '')
    {
        $config = self::getConfig(!empty($configKey) ? $configKey : 'alipay');

        !empty($notifyUrl) && $config['notify_url'] = $notifyUrl;

        $this->payIns = Pay::alipay($config);
    }

    /**
     * @param $configKey
     *
     * @return \Illuminate\Config\Repository|mixed
     * @throws \Exception
     */
    public static function getConfig($configKey)
    {
        $config = config("pay.".$configKey);

        if (empty($config)) throw new \Exception("配置不存在");

        return $config;
    }

    /**
     * 电脑支付
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function webPay(Request $request)
    {
        $rules = [
            'out_trade_no' => 'required',
            'total_amount' => 'required',
            'subject'      => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'out_trade_no' => $request['out_trade_no'],
            'total_amount' => intval($request['total_amount'] * 100),
            'subject'      => $request['subject'],
        ];

        return $this->payIns->web($order);
    }

    /**
     * 手机网站支付
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function wapPay(Request $request)
    {
        $rules = [
            'out_trade_no' => 'required',
            'total_amount' => 'required',
            'subject'      => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'out_trade_no' => $request['out_trade_no'],
            'total_amount' => intval($request['total_amount'] * 100),
            'subject'      => $request['subject'],
        ];

        return $this->payIns->wap($order);
    }

    /**
     * APP支付
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function appPay(Request $request)
    {
        $rules = [
            'out_trade_no' => 'required',
            'total_amount' => 'required',
            'subject'      => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'out_trade_no' => $request['out_trade_no'],
            'total_amount' => intval($request['total_amount'] * 100),
            'subject'      => $request['subject'],
        ];

        return $this->payIns->app($order);
    }

    /**
     * 刷卡支付
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function posPay(Request $request)
    {
        $rules = [
            'out_trade_no' => 'required',
            'total_amount' => 'required',
            'subject'      => 'required',
            'auth_code' => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'out_trade_no' => $request['out_trade_no'],
            'total_amount' => intval($request['total_amount'] * 100),
            'subject'      => $request['subject'],
            'auth_code' => $request['auth_code'],
        ];

        return $this->payIns->pos($order);
    }

    /**
     * 扫码支付
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function scanPay(Request $request)
    {
        $rules = [
            'out_trade_no' => 'required',
            'total_amount' => 'required',
            'subject'      => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'out_trade_no' => $request['out_trade_no'],
            'total_amount' => intval($request['total_amount'] * 100),
            'subject'      => $request['subject'],
        ];

        // 二维码内容： $qr = $result->qr_code;
        return $this->payIns->scan($order);
    }

    /**
     * 账户转账
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function transferPay(Request $request)
    {
        $rules = [
            'out_biz_no' => 'required',
            'trans_amount' => 'required',
            'product_code' => 'required',
            'payee_info' => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $payeeInfo = $request['payee_info'];

        $order = [
            'out_biz_no' => $request['out_biz_no'],
            'trans_amount' => intval($request['trans_amount'] * 100),
            'product_code' => $request['product_code'],
            'payee_info' => [
                'identity' => $payeeInfo['identity'],
                'identity_type' => $payeeInfo['identity_type']
            ],
        ];

        return $this->payIns->transfer($order);
    }

    /**
     * 小程序支付
     *
     * @param Request $request
     *
     * @return \Yansongda\Supports\Collection
     * @throws \Exception
     */
    public function miniPay(Request $request)
    {
        $rules = [
            'out_trade_no' => 'required',
            'total_amount' => 'required',
            'subject'      => 'required',
            'buyer_id' => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'out_trade_no' => $request['out_trade_no'],
            'total_amount' => intval($request['total_amount'] * 100),
            'subject'      => $request['subject'],
            'buyer_id' => $request['buyer_id'],
        ];

        return $this->payIns->mini($order);
    }

    /**
     * 退款
     *
     * @return \Yansongda\Supports\Collection
     * @throws \Yansongda\Pay\Exceptions\GatewayException
     * @throws \Yansongda\Pay\Exceptions\InvalidConfigException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     */
    public function refund()
    {
        $rules = [
            'out_trade_no' => 'required',
            'refund_amount' => 'required',
        ];

        if ($err = ValidatorService::validateParams($rules)) throw new \Exception($err);

        $order = [
            'out_trade_no' => $request['out_trade_no'],
            'refund_amount' => intval($request['refund_amount'] * 100)
        ];

        return $this->payIns->refund($order);
    }

    public function notify($configKey = 'wechat', $notifyUrl = '', \Closure $logic)
    {
        $alipay = (new self($configKey, $notifyUrl))->payIns;

        try{
            $data = $alipay->verify(); // 是的，验签就这么简单！

            // TODO
            // 请自行对 trade_status 进行判断及其它逻辑进行判断，在支付宝的业务通知中，只有交易通知状态为 TRADE_SUCCESS 或 TRADE_FINISHED 时，支付宝才会认定为买家付款成功。
            // 1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号；
            // 2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额）；
            // 3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）；
            // 4、验证app_id是否为该商户本身。
            // 5、其它业务逻辑情况

            Log::debug('Alipay notify', $data->all());
        } catch (\Exception $e) {
            // $e->getMessage();
        }

        return $alipay->success()->send();// laravel 框架中请直接 `return $alipay->success()`
    }

}

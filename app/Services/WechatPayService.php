<?php
namespace App\Services;

use EasyWeChat\Factory;
use function EasyWeChat\Kernel\Support\generate_sign;

class WechatPayService
{
    /**
     * 申请了支付的小程序appid
     *
     * @var
     */
    public $appId;

    /**
     * 小程序支付实例
     *
     * @var \EasyWeChat\Payment\Application
     */
    public $wxpay;

    /**
     * 获取实例
     *
     * @return WechatPayService
     * @throws \Exception
     */
    public static function getInstance($appId = null)
    {
        $config= self::getPaymentConfig($appId);

        if (empty($config)) throw new \Exception('payment config not found');

        if (empty($config['app_id'])) throw new \Exception('payment config app_id not found');

        return new self($config['app_id']);
    }

    /**
     * 根据appId获取小程序支付配置
     * 根据header中的referer判断请求来源于哪个小程序
     *
     * @param null $appId
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    public static function getPaymentConfig($appId = null)
    {
        $appId = !empty($appId) ? $appId : XcxService::getAppid();

        return config("wechat.payment.".$appId);
    }

    /**
     * 获取sdk支付实例
     *
     * @param null $appId
     *
     * @return \EasyWeChat\Payment\Application
     * @throws \Exception
     */
    public static function getApp($appId = null)
    {
        return self::getInstance($appId)->wxpay;
    }

    /**
     * WechatPayService constructor.
     *
     * @param $appId
     */
    private function __construct($appId)
    {
        $this->appId = $appId;
        $this->wxpay = app('wechat.payment.'.$appId);
    }

    /**
     * 统一下单
     *
     * @param $openid
     * @param $totalFee
     * @param $body
     * @param $outTradeNo
     * @param $notifyUrl
     *
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function prepay($openid, $totalFee, $body, $outTradeNo, $notifyUrl)
    {
        $result = $this->wxpay->order->unify([
            'body'         => $body, // 你自己想写的名称
            'out_trade_no' => $outTradeNo, // 你自己定义的订单号
            'trade_type'   => 'JSAPI',  // 必须为JSAPI
            'openid'       => $openid, // 这里的openid为付款人的openid
            'total_fee'    => $totalFee * 100, // 总价
            'notify_url' => $notifyUrl // 支付结果回调
        ]);

        // 如果成功生成统一下单的订单，那么进行二次签名
        if ($result['return_code'] === 'SUCCESS') {
            // 二次签名的参数必须与下面相同
            $params = [
                'appId'     => $this->appId,
                'timeStamp' => time(),
                'nonceStr'  => $result['nonce_str'],
                'package'   => 'prepay_id=' . $result['prepay_id'],
                'signType'  => 'MD5',
            ];

            // config('wechat.payment.default.key')为商户的key
            $params['paySign'] = generate_sign($params, config('wechat.payment.'.$this->appId.'.key'));

            return $params;
        } else {
            return $result;
        }

    }

    /**
     * 支付回调
     *
     * @param $logic 业务逻辑
     *
     * @return mixed
     */
    public function callback(\Closure $logic)
    {
        return $this->wxpay->handlePaidNotify(
            function ($message, $fail) use ($logic) {
                if ($message['result_code'] === 'FAIL') {
                    logger()->warning('WXPAY_CALLBACK', ['FAIL', $message]);
                    return true;
                } else if ($message['return_code'] === 'SUCCESS') {
                    // 业务逻辑
                    $logic($message);
                    return true;
                }
            }
        );
    }

    /**
     * 根据商户订单号查询订单支付状态
     *
     * @param $outTradeNo
     *
     * @return mixed
     */
    public function queryByOutTradeNumber($outTradeNo)
    {
        return $this->wxpay->order->queryByOutTradeNumber($outTradeNo);
    }

    /**
     * 根据微信订单号查询订单支付状态
     *
     * @param $transaction_id
     *
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function queryByTransactionId($transactionId)
    {
        return $this->wxpay->order->queryByTransactionId($transactionId);
    }

    /**
     * 退款 需要用到证书
     *
     * @param               $outTradeNo 商户订单号
     * @param               $refundNumber 商户退款单号，自己生成用于自己识别即可
     * @param int           $totalFee 已支付金额(单位:分)
     * @param int           $refundFee 退款金额(单位:分)
     * @param array         $config 其他参数
     * @param \Closure|null $logic 退款成功的业务逻辑
     *
     * @return bool
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function refund($outTradeNo, $refundNumber, int $totalFee, int $refundFee, array $config, \Closure $logic)
    {
        // 查询是否已支付
        $payStatus = $this->queryByOutTradeNumber($outTradeNo);
        if (empty($payStatus['trade_state']) || $payStatus['trade_state'] != 'SUCCESS') {
            throw new \Exception("未支付，不能退款");
        }

        // 退款
        $res = $this->wxpay->refund->byOutTradeNumber($outTradeNo, $refundNumber, $totalFee, $refundFee, $config);

        if ($res['return_code'] == 'FAIL') throw new \Exception($res['return_msg']);

        if ($res['return_code'] == 'SUCCESS' && $res['result_code'] == 'SUCCESS') {

            try {
                \DB::beginTransaction();
                $logic($income);
            } catch(\Exception $e) {
                \DB::rollback();
                throw new \Exception($e->getMessage());
            }
            \DB::commit();
            return true;
        } else {
            throw new \Exception($res['err_code_des']);
        }

        return true;
    }

}

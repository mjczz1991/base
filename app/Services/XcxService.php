<?php

namespace App\Services;

use App\Model\WechatUser;
use EasyWeChat\Factory;

class XcxService
{
    /**
     * 小程序实例
     *
     * @var \EasyWeChat\MiniProgram\Application
     */
    public $app;

    /**
     * 获取实例
     *
     * @return \App\Services\WeChat\XcxService
     * @throws \Exception
     */
    public static function getInstance()
    {
        $config= self::getXcx();

        if (empty($config)) throw new \Exception('xcx not found');

        return new self($config);
    }

    /**
     * 根据header中的referer判断请求来源于哪个小程序
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    public static function getXcx()
    {
        return config("wechat.mini_program.".self::getAppid());
    }

    /**
     * 根据header中的referer获取小程序appid
     *
     * @return mixed
     */
    public static function getAppid()
    {
        $referer = \Request::header('referer');
        $appId = explode('/', trim(parse_url($referer)['path'], '/'))[0];

        // 从请求参数获取小程序
        empty($appId) && $appId = env(request("xcx"));

        if (empty($appId)) throw new \Exception('xcx appid not found');

        return $appId;
    }

    /**
     * 获取sdk的app实例
     *
     * @return \EasyWeChat\MiniProgram\Application
     * @throws \Exception
     */
    public static function getApp()
    {
        return self::getInstance()->app;
    }

    /**
     * XcxService constructor.
     *
     * @param array $config
     */
    public function __construct ($config = [])
    {
        $this->app = Factory::miniProgram($config);
    }

    /**
     * 解密微信信息
     *
     * @param $iv
     * @param $encryptData
     * @param $open_id
     *
     * @return array
     * @throws \EasyWeChat\Kernel\Exceptions\DecryptException
     * @throws \Exception
     */
    public function decryptData($iv, $encryptData, $session_key)
    {
        return $this->app->encryptor->decryptData($session_key, $iv, $encryptData);
    }

    public function getOpenid($code)
    {
        $miniProgramRs = $this->app->auth->session($code);

        if (isset($miniProgramRs['errcode'])) {
            throw new \Exception($miniProgramRs['errmsg']);
        }

        return $miniProgramRs['openid'];
    }

}

<?php

use App\Http\Resources\UserResource;
use App\Services\ConfigService;
use App\Services\DbService;
use App\User;
use Illuminate\Support\Facades\Route;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

$url = "http://gg.mjczz.shop/wx_user/oauth_profile";


Route::get('wx_user-oauth_profile', 'WechatUserController@oauthProfile');
// callback：OAuth授权完成后的回调页地址(如果使用中间件，则随便填写。。。)
Route::get('wx_user-oauth_callback', 'WechatUserController@oauthCallback');

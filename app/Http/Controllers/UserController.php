<?php

namespace App\Http\Controllers;

use App\Http\Controllers\V1\BaseController;
use Illuminate\Http\Request;
use App\User;
use Encore\Admin\Auth\Database\Administrator;
use App\Http\Resources\AdminUserResource;
use App\Http\Resources\UserResource;

class UserController extends BaseController
{
    public function listUser(Request $request)
    {
        return self::response(UserResource::collection(User::all()));
    }

    public function listAdminUser(Request $request)
    {
        return self::response(AdminUserResource::collection(Administrator::all()));
    }
}

<?php
/**
 * Created by czz.
 * User: czz
 * Date: 2019/11/28
 * Time: 10:37
 */


namespace App\Services;

use Encore\Admin\Config\Config;
use Encore\Admin\Config\ConfigModel;
use Illuminate\Support\Facades\Schema;
use function json_decode;

class ConfigService
{

    /**
     * 获取配置
     *
     * @param       $name
     * @param mixed ...$default
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    public static function get($name = '', ...$default)
    {
        $table = config('admin.extensions.config.table', 'admin_config');

        if (Schema::hasTable($table)) Config::load();


        return !empty($default) ? config($name, $default[0]) : config($name);
    }

    public static function getCacheOptions()
    {
        $options = ConfigModel::all(['name', 'value']);

        $options->each(function($val) {
            $val = json_decode($val, 1);
        });

        return $options;
    }

}

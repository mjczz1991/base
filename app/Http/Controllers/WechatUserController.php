<?php

namespace App\Http\Controllers;

use App\Http\Controllers\V1\BaseController;
use EasyWeChat\Factory;
use Illuminate\Http\Request;

class WechatUserController extends BaseController
{
    public function oauthProfile(Request $request)
    {
        $app = Factory::officialAccount(config("wechat.official_account.default"));
        $oauth = $app->oauth;

        $wechatUser = session("wechat_user");

        // 未登录
        if (empty($wechatUser['id'])) {
            session(['target_url' => '/wx_user-oauth_profile']);

            return $oauth->redirect();
        }

        dd($wechatUser);
    }

    public function oauthCallback(Request $request)
    {
        $app = Factory::officialAccount(config("wechat.official_account.default"));
        $oauth = $app->oauth;

        // 获取 OAuth 授权结果用户信息
        $user = $oauth->user();
        if (empty($user)) return "获取用户信息失败";

        session(['wechat_user' => $user->toArray()]);
        if (empty(session('wechat_user'))) return "用户信息无法存储";

        $targetUrl = session('target_url', '/wx_user-oauth_profile');

        header('location:'. $targetUrl); // 跳转到 /wx_user-oauth_profile
    }

}

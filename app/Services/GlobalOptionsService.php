<?php

namespace App\Services;

use Encore\Admin\Config\ConfigModel;
use function json_decode;

class GlobalOptionsService
{
    /**
     * 获取数据字典
     *
     * @param null $key
     *
     * @return array|bool|null
     */
    public static function getOptions($key = null)
    {
        $options = ConfigModel::all(['name', 'value']);

        $arr = [];
        foreach ($options as &$item) {
            $arr[$item['name']] = json_decode($item['value'], 1);
        }

        if (empty($key)) return $arr;

        return isset($arr[$key]) ? $arr[$key] : [];
    }

}

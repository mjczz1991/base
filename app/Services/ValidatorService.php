<?php
/**
 * Created by lebron.
 * User: lebron
 * Date: 2020/3/8
 * Time: 14:46
 */

namespace App\Services;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;

class ValidatorService
{
    /**
     * 验证请求参数
     *
     * @param array $rules 规则
     *
     * @return mixed
     */
    public static function validateParams($rules = [])
    {
        $validator = Validator::make(Request::all(), $rules);

        if ($validator->fails()) return $validator->messages()->first();
    }
}
